'use strict';

const Alexa = require("alexa-sdk");
const AWS = require("aws-sdk");

const APP_ID = "amzn1.ask.skill.a75568a1-5c9d-4b1e-8c37-27abd009a041";
const AWSregion = 'us-east-1'; // us-east-1
const TABLE_NAME = 'JollibeeAutomatedOrdersDb';
const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

const tableNumberPrompt = "Please let me know your table number to proceed";
const foodSizePrompt = "What size for";
const orderPrompt = "What would you like to have?";
const repeatPrompt = "Please repeat your order";
const didNotUnderstandPrompt = "I did not understand your order, " + repeatPrompt;
const moreOrderPrompt = "Would you like anything else?";
const correctOrderPrompt = "Is this correct?";
const multipleMenuItemsMatchPrompt = "Several menu items matched your order. Please choose from";
const welcomeMsg = "Hi, Welcome to Jollibee";
const orderOutput = "You ordered";


const returnDefaultEvent = (event) => Object.assign({}, {
    request: {
      locale: 'en-US',
      type: 'LaunchRequest'
    },
    session: {
      application: {
        applicationId: 'applicationId'
      },
      user: {
        userId: 'userId'
      }
    }
  },
  event
);


exports.handler = (event, context) => {
  const alexa = Alexa.handler(returnDefaultEvent(event), context);
  alexa.APP_ID = APP_ID;
  alexa.registerHandlers(handlers, orderConfirmationHandler, nextOrderOrCompleteHandler);
  //alexa.dynamoDBTableName = TABLE_NAME;
  alexa.execute();
};




const handlers = {
  //By having a single 'Unhandled' handler, we ensure all requests are route to it
  'Unhandled': function() {
    //log the event sent by the Alexa Service in human readable format
    console.log(JSON.stringify(this.event));
    let skillId, requestType, dialogState, intent, intentName, intentConfirmationStatus, slotArray, slots, count;

    try {
      //Parse necessary data from JSON object using dot notation
      //build output strings and check for undefined
      skillId = this.event.session.application.applicationId;
      requestType = "The request type is, " + this.event.request.type + " .";
      dialogState = this.event.request.dialogState;
      intent = this.event.request.intent;
      if (intent != undefined) {
        intentName = " The intent name is, " + this.event.request.intent.name + " .";
        slotArray = this.event.request.intent.slots;
        intentConfirmationStatus = this.event.request.intent.confirmationStatus;

        if (intentConfirmationStatus != "NONE" && intentConfirmationStatus != undefined) {
          intentConfirmationStatus = " and its confirmation status is " + intentConfirmationStatus + " . ";
          intentName = intentName + intentConfirmationStatus;
        }
      }
      else {
        intentName = "";
        slotArray = "";
        intentConfirmationStatus = "";
      }

      slots = "";
      count = 0;

      if (slotArray == undefined || slots == undefined) {
        slots = "";
      }

      //Iterating through slot array
      for (let slot in slotArray) {
        count += 1;
        let slotName = slotArray[slot].name;
        let slotValue = slotArray[slot].value;
        let slotConfirmationStatus = slotArray[slot].confirmationStatus;
        slots = slots + "The <say-as interpret-as='ordinal'>" + count + "</say-as> slot is, " + slotName + ", its value is, " + slotValue;

        if (slotConfirmationStatus != undefined && slotConfirmationStatus != "NONE") {
          slots = slots + " and its confirmation status is " + slotConfirmationStatus + " . ";
        }
        else {
          slots = slots + " . ";
        }
      }

      //Delegate to Dialog Manager when needed
      //<reference to docs>
      if (dialogState == "STARTED" || dialogState == "IN_PROGRESS") {
        this.emit(":delegate");
      }
    }
    catch (err) {
      console.log("Error: " + err.message);
    }

    let speechOutput = "Your end point received a request, here's a breakdown. " + requestType + " " + intentName + slots;
    let cardTitle = "Skill ID: " + skillId;
    let cardContent = speechOutput;

    this.response.cardRenderer(cardTitle, cardContent);
    this.response.speak(speechOutput);
    this.emit(':responseReady');
  },
  'GetTableNumberIntent': function() {
    console.log("in GetTableNumberIntent");
    let tableNumber = getTableNumberFromSlot.call(this);
    if (tableNumber > 0) {
      setTableNumberToSession.call(this, tableNumber);

      let response = 'You are in table ' + tableNumber;
      //if there's no order yet
      if (!hasFinalOrderListInSession.call(this)) {
        response += ", " + orderPrompt;
        this.response.speak(response).listen(orderPrompt);
      }
      else {
        this.response.speak(response).listen(moreOrderPrompt);
      }
      this.response.shouldEndSession = false;
    }
    else {
      this.response.speak(tableNumberPrompt).listen(tableNumberPrompt);
    }
    this.emit(":responseReady");
  },
  'GetFoodSizeIntent': function() {
    let tempOrder = getTempOrder.call(this);

    if (tempOrder) {
      updateTempOrder.call(this, { foodSize: this.event.request.intent.slots.foodSize.value });
      validateOrder.call(this);
    }
    else {
      this.response.speak(didNotUnderstandPrompt).listen(didNotUnderstandPrompt);
      this.emit(':responseReady');
    }
  },
  'LaunchRequest': function() {
    setTableNumberToSession.call(this, 0);
    initFinalOrderListInSession.call(this);
    this.emit('WelcomeCustomer');
  },
  'WelcomeCustomer': function() {
    this.response.speak(welcomeMsg + ", " + tableNumberPrompt).listen(tableNumberPrompt);
    this.emit(':responseReady');
  },
  'OrderIntent': function() {
    //delegate to Alexa to collect all the required slot values
    let filledSlots = delegateSlotCollection.call(this);

    let foodCount = getFoodCountFromSlot.call(this);
    let food = '';

    let matchedMenuItems = getMatchedMenuItems.call(this);
    //multiple matches
    if (matchedMenuItems.length > 1) {
      let multipleItemsPrompt = multipleMenuItemsMatchPrompt;
      for (let i = 0; i < matchedMenuItems.length; i++) {
        multipleItemsPrompt += ", " + matchedMenuItems[i].name;
      }
      this.response.speak(multipleItemsPrompt).listen(multipleItemsPrompt);
      this.emit(':responseReady');
    }
    else {
      //console.log('matchedMenuItems id 1: ' + matchedMenuItems[0].id);
      food = matchedMenuItems[0];
    }




    //process food if we need to ask for its size
    let foodSize = getFoodSizeFromSlot.call(this);
    //console.log('food id 1: ' + food.id);
    //console.log('foodSize: ' + foodSize);
    console.log('food 1: ' + food);
    console.log('askForFoodSize: ' + askForFoodSize(food));
    if (!foodSize && askForFoodSize(food)) {
      let orderObject = createOrderObject(foodCount, food, '');
      //make order available to the next intent
      storeTempOrder.call(this, orderObject);
      this.response.speak(foodSizePrompt + ", " + food.name + " ?").listen(foodSizePrompt + ", " + food.name + " ?");
      this.emit(':responseReady');
    }
    else {
      let orderObject = createOrderObject(foodCount, food, foodSize);
      //make order available to the next intent
      storeTempOrder.call(this, orderObject);
      validateOrder.call(this);

    }

  },

  'AMAZON.HelpIntent': function() {
    orderOutput = "";
    welcomeReprompt = "";
    //this.response.speak(orderOutput).listen(welcomeReprompt);
    this.response.speak('AMAZON help Intent');
    this.emit(':responseReady');
  },
  'AMAZON.CancelIntent': function() {
    orderOutput = "";
    this.response.speak('AMAZON cancel Intent');
    this.emit(':responseReady');
  },
  'AMAZON.StopIntent': function() {
    orderOutput = "";
    this.response.speak('AMAZON Stop Intent');
    this.emit(':responseReady');
  },
  'SessionEndedRequest': function() {
    var speechOutput = "";
    this.response.speak('Session Ended Request');
    this.emit(':responseReady');
  },
};

const orderConfirmationHandler = Alexa.CreateStateHandler("ORDER_CONFIRM", {
  'AMAZON.YesIntent': function() {
    let tempOrder = getTempOrder.call(this);

    if (tempOrder) {
      let $this = this;
      storeCustomerOrderToSession.call(this, tempOrder);
      removeTempOrder.call(this);
    }

    let tableNumber = getTableNumberFromSession.call(this);
    if (!tableNumber) {
      this.response.speak(tableNumberPrompt).listen(tableNumberPrompt);
    }
    else {
      this.handler.state = "NEXT_ORDER_OR_COMPLETE_SESSION";
      this.response.speak(moreOrderPrompt).listen(moreOrderPrompt);
    }
    this.emit(':responseReady');
  },
  'AMAZON.NoIntent': function() {
    removeTempOrder.call(this);
    resetState.call(this);
    this.response.speak(repeatPrompt).listen(repeatPrompt);
    this.emit(':responseReady');
  },
  'Unhandled': function() {
    this.response
      .speak('Messages.UNHANDLED')
      .listen('Messages.HELP');
    this.emit(':responseReady');
  }
})

const nextOrderOrCompleteHandler = Alexa.CreateStateHandler("NEXT_ORDER_OR_COMPLETE_SESSION", {
  'AMAZON.YesIntent': function() {
    resetState.call(this);
    this.response.speak(orderPrompt).listen(orderPrompt);
    this.emit(':responseReady');
  },
  'AMAZON.NoIntent': function() {
    if (hasFinalOrderListInSession.call(this)) {
      let orders = getFinalOrderListFromSession.call(this);
      let orderListPrompt = '';


      for (let i = 0; i < orders.length; i++) {
        let orderLine = orders[i].foodCount;

        if (orders[i].foodSize) {
          orderLine += ", " + orders[i].foodSize;
        }
        orderLine += ", " + orders[i].food.name;

        orderListPrompt += orderLine + ", ";
      }

      let finalOrderFromSession = getFinalOrderListFromSession.call(this);
      console.log('finalOrderFromSession: ' + JSON.stringify(finalOrderFromSession));

      var $this = this;
      storeFinalOrderToDynamo.call(this, finalOrderFromSession, function(err, orderNumber) {
        let msg = "";
        if (err) {
          msg = "I'm sorry. There was an error encountered in taking your order. Please wait for a service crew to assist you.";
        }
        else {
          msg = 'You ordered, ' + orderListPrompt + ", Your order number is " +  orderNumber + ". Your order is being processed. Thank you for dining at Jollibee. Langhap Sarap!";
        }
        $this.response.speak(msg);
        $this.emit(':responseReady');
      });
    }
  },
  'Unhandled': function() {
    this.response
      .speak('Messages.UNHANDLED')
      .listen('Messages.HELP');
    this.emit(':responseReady');
  }
})

function getFinalOrderListFromSession() {
  return this.attributes['orders'];
}

function storeToFinalOrderListInSession(orderObject) {
  this.attributes['orders'].push(orderObject);
}

function initFinalOrderListInSession() {
  if (!hasFinalOrderListInSession.call(this)) {
    this.attributes['orders'] = [];
  }
}

function hasFinalOrderListInSession() {
  return this.attributes.hasOwnProperty('orders') && this.attributes['orders'].length > 0;
}

function getTableNumberFromSlot() {
  return this.event.request.intent.slots.tableNumber && this.event.request.intent.slots.tableNumber.value ? this.event.request.intent.slots.tableNumber.value : 0;
}

function getFoodCountFromSlot() {
  return this.event.request.intent.slots.foodCount && this.event.request.intent.slots.foodCount.value ? this.event.request.intent.slots.foodCount.value : 0;
}

function getFoodSizeFromSlot() {
  return this.event.request.intent.slots.foodSize && this.event.request.intent.slots.foodSize.value ? this.event.request.intent.slots.foodSize.value : '';
}


function askForFoodSize(food) {
  console.log('food 2: ' + food);
  //console.log('foodId: ' + food.id);
  if (food) {
    if (food.id === 'JOLLY_CRISPY_FRIES') {
      return true;
    }
  }
  return false;
}


function resetState() {
  this.handler.state = '' // delete this.handler.state might cause reference errors
  delete this.attributes['STATE'];
}



function getMatchedMenuItems() {
  let matchedMenuItems = [];

  if (this.event.request.intent.slots.food &&
    this.event.request.intent.slots.food.resolutions &&
    this.event.request.intent.slots.food.resolutions.resolutionsPerAuthority.length > 0) {
    console.log('in getMatchedMenuItems if');
    for (let i = 0; i < this.event.request.intent.slots.food.resolutions.resolutionsPerAuthority.length; i++) {
      if (this.event.request.intent.slots.food.resolutions.resolutionsPerAuthority[i].values.length > 0) {
        console.log('in getMatchedMenuItems if 2:');
        for (let j = 0; j < this.event.request.intent.slots.food.resolutions.resolutionsPerAuthority[i].values.length; j++) {
          let value = this.event.request.intent.slots.food.resolutions.resolutionsPerAuthority[i].values[j].value;
          matchedMenuItems.push({ name: value.name, id: value.id });
        }
      }
    }
  }
  return matchedMenuItems;
}

function setTableNumberToSession(tableNumber) {
  this.attributes['tableNumber'] = tableNumber;
}

function getTableNumberFromSession() {
  return this.attributes['tableNumber'];
}

function storeTempOrder(order) {
  this.attributes['temp_order'] = order;
}

function getTempOrder() {
  return this.attributes['temp_order'];
}

function updateTempOrder(order) {
  if (this.attributes.hasOwnProperty('temp_order')) {
    Object.assign(this.attributes['temp_order'], order);
  }
  else {
    storeTempOrder.call(this, order);
  }
}

function removeTempOrder() {
  delete this.attributes['temp_order'];
}

function createOrderObject(foodCount, food, foodSize) {
  return {
    foodCount: foodCount,
    food: food,
    foodSize: foodSize
  }
}

function validateOrder() {
  let tempOrder = getTempOrder.call(this);
  if (tempOrder) {
    let orderPrompt = orderOutput + ", " + tempOrder.foodCount;
    if (tempOrder.foodSize) {
      orderPrompt += ", " + tempOrder.foodSize;
    }
    orderPrompt += ", " + tempOrder.food.name + ", " + correctOrderPrompt;
    this.handler.state = "ORDER_CONFIRM";
    this.response.speak(orderPrompt).listen(orderPrompt);

    this.emit(":responseReady");
  }
}

function storeCustomerOrderToSession(orderObject, callback) {
  initFinalOrderListInSession.call(this);
  let finalOrder = Object.assign({}, orderObject);
  storeToFinalOrderListInSession.call(this, finalOrder);
}

function storeFinalOrderToDynamo(finalOrder, callback) {
  AWS.config.update({ region: AWSregion });


  console.log('putting orders to DynamoDB table');
  console.log('this: ' + JSON.stringify(this));

  let now = new Date();
  let milli = now.getTime();
  let orderNumber = now.getHours().toString() + now.getMinutes().toString() + now.getSeconds();

  let userId = this.event.session.user.userId;
  

  console.log('timestamp: ' + milli);
  console.log('userId: ' + userId);
  console.log('orderNumber: ' + orderNumber);
  console.log('finalOrder: ' + JSON.stringify(finalOrder));

  dynamodb.putItem({
    TableName: TABLE_NAME,
    Item: {
      ActiveYn: {
        N: '1'
      },
      UserId: {
        S: userId
      },
      TableNumber: {
        N: getTableNumberFromSession.call(this).toString()
      },
      CreatedDateInMilli: {
        N: milli.toString()
      },
      OrderStatus: {
        S: 'ORDERED'
      },
      OrderNumber: {
        N: orderNumber
      },
      Order: {
        S: JSON.stringify(finalOrder)
      }
    }
  }, function(err, data) {
    if (err) {
      console.log(err, err.stack);
    }
    else {

    }
    if (callback) {
      callback(err, orderNumber);
    }

  });





}

function delegateSlotCollection() {
  console.log("in delegateSlotCollection");
  console.log("current dialogState: " + this.event.request.dialogState);
  if (this.event.request.dialogState === "STARTED") {
    console.log("in Beginning");
    var updatedIntent = this.event.request.intent;
    console.log("in STARTED: " + JSON.stringify(updatedIntent));
    //optionally pre-fill slots: update the intent object with slot values for which
    //you have defaults, then return Dialog.Delegate with this updated intent
    // in the updatedIntent property
    this.emit(":delegate", updatedIntent);
  }
  else if (this.event.request.dialogState !== "COMPLETED") {
    console.log("in not completed");
    console.log("in !== COMPLETED: " + JSON.stringify(this.event.request.intent));
    // return a Dialog.Delegate directive with no updatedIntent property.
    this.emit(":delegate");
  }
  else {
    console.log("in completed");
    console.log("returning: " + JSON.stringify(this.event.request.intent));
    // Dialog is now complete and all required slots should be filled,
    // so call your normal intent handler.
    return this.event.request.intent;
  }
}
