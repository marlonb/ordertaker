{
    "languageModel": {
      "types": [
        {
          "name": "FOOD_SIZE",
          "values": [
            {
              "id": null,
              "name": {
                "value": "Regular",
                "synonyms": [
                  "Small"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Medium",
                "synonyms": []
              }
            },
            {
              "id": null,
              "name": {
                "value": "Large",
                "synonyms": []
              }
            }
          ]
        },
        {
          "name": "JOLLI_MENU",
          "values": [
            {
              "id": null,
              "name": {
                "value": "Breakfast Joys Corned Beef",
                "synonyms": [
                  "Corned Beef",
                  "Breakfast",
                  "Breakfast Corned Beef"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Breakfast Joys Beef Tapa",
                "synonyms": [
                  "Tapa",
                  "Breakfast",
                  "Beef Tapa",
                  "Beef",
                  "Tapsilog",
                  "Tapsi"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Breakfast Joys Breakfast Steak",
                "synonyms": [
                  "Steak",
                  "Breakfast",
                  "Breakfast Steak"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Breakfast Joys Hotdog",
                "synonyms": [
                  "Hotdog",
                  "Breakfast",
                  "Sausage",
                  "Hotsilog"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Breakfast Joys Longganisa",
                "synonyms": [
                  "Longsilog",
                  "Longganisa",
                  "Sausage",
                  "Breakfast"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Spaghetti",
                "synonyms": [
                  "Spag",
                  "Jollibee Spaghetti",
                  "Jolly Spaghetti",
                  "Spaghetti"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Palabok Fiesta",
                "synonyms": [
                  "Palabok"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Garlic Pepper Beef",
                "synonyms": [
                  "Beef",
                  "Garlic Beef",
                  "Pepper Beef",
                  "Garlic Pepper"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Ultimate Burger Steak",
                "synonyms": [
                  "Large Burger Steak",
                  "Burger Steak"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "All Day Garlic Bangus",
                "synonyms": [
                  "Bangus",
                  "Garlic Bangus"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Burger Steak",
                "synonyms": [
                  "Steak"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Five Piece Shanghai Rolls",
                "synonyms": [
                  "Shanghai",
                  "Shanghai Rolls"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Buttered Corn",
                "synonyms": [
                  "Corn"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Peach Mango Pie",
                "synonyms": [
                  "Pie",
                  "Mango Pie",
                  "Peach Pie",
                  "Peach Mango"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Jolly Hotdog Classic",
                "synonyms": [
                  "Hotdog",
                  "Classic Hotdog",
                  "Jolly Hotdog"
                ]
              }
            },
            {
              "id": "JOLLY_CRISPY_FRIES",
              "name": {
                "value": "Jolly Crispy Fries",
                "synonyms": [
                  "Fries",
                  "French Fries",
                  "Jolly Fries",
                  "Crispy Fries"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Creamy Macaroni Soup",
                "synonyms": [
                  "Macaroni",
                  "Soup",
                  "Sopas",
                  "Creamy Sopas",
                  "Creamy Macaroni"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Yum With TLC Sesame Seed",
                "synonyms": [
                  "Yum",
                  "Burger",
                  "TLC",
                  "Yum with TLC",
                  "Burger With TLC"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Cheesy Bacon Mushroom Yum",
                "synonyms": [
                  "Yum",
                  "Burger",
                  "Bacon Yum",
                  "Mushroom Yum",
                  "Cheesy Bacon Yum"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Cheesy Bacon Mushroom Champ",
                "synonyms": [
                  "Champ",
                  "Cheese Champ",
                  "Mushroom Champ",
                  "Bacon Champ",
                  "Burger"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Champ One Third Pound Beef Patty",
                "synonyms": [
                  "Champ",
                  "Burger",
                  "One Third Pound Champ",
                  "One Third Pound Burger"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Yum With Cheese",
                "synonyms": [
                  "Yum",
                  "Regular Yum",
                  "Regular Yum With Cheese",
                  "Burger",
                  "Regular Burger",
                  "Burger With Cheese",
                  "Regular Burger With Cheese"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Two Piece Chicken",
                "synonyms": [
                  "Two Piece Chicken Joy",
                  "Two Piece ChickenJoy",
                  "Two Pieces Chicken",
                  "Chicken"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Chicken Joy",
                "synonyms": [
                  "Regular Chicken Joy",
                  "Regular ChickenJoy",
                  "ChickenJoy",
                  "Chicken"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Spicy Chicken Joy",
                "synonyms": [
                  "Chicken Joy",
                  "ChickenJoy",
                  "Chicken",
                  "Spicy Chicken",
                  "Spicy ChickenJoy"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Chicken Joy Bucket",
                "synonyms": [
                  "Chicken",
                  "Bucket",
                  "ChickenJoy",
                  "Chicken Joy"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Strawberry Shake",
                "synonyms": [
                  "Shake"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Choco Shake",
                "synonyms": [
                  "Shake",
                  "Chocolate Shake"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Kitkat Mix Ins",
                "synonyms": [
                  "KitKat"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Strawberry Sundae",
                "synonyms": [
                  "Sundae"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Chocolate Sundae",
                "synonyms": [
                  "Choco Sundae",
                  "Sundae"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Coke Float",
                "synonyms": [
                  "Float"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Cone Twirl Crunchy Chocolate",
                "synonyms": [
                  "Twirl",
                  "Chocolate Twirl",
                  "Chocolate Cone",
                  "Cone Twirl",
                  "Crunchy Twirl",
                  "Crunchy Chocolate",
                  "Crunchy Choco"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Cone Twirl Vanilla",
                "synonyms": [
                  "Cone Twirl",
                  "Vanilla",
                  "Vanilla Cone",
                  "Vanilla Twirl",
                  "Twirl"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Jolly Kids Meal Spaghetti",
                "synonyms": [
                  "JKM",
                  "Jolly Kiddie Meal",
                  "Jolly Kids Meal",
                  "Kids Spaghetti",
                  "Kids Spag",
                  "Kiddie Spaghetti",
                  "Kiddie Spag",
                  "Spaghetti",
                  "Spag",
                  "JKM Spag",
                  "JKM Spaghetti",
                  "Kids Meal Spaghetti",
                  "Kids Spaghetti",
                  "Kids Spag",
                  "Kids Meal Spaghetti"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Jolly Kids Meal Yum",
                "synonyms": [
                  "JKM",
                  "Jolly Kiddie Meal",
                  "Yum",
                  "Burger",
                  "Kiddie Yum",
                  "Kiddie Burger",
                  "Kids Yum",
                  "Kids Burger",
                  "Kiddie Meal Yum",
                  "Kiddie Meal Burger",
                  "Jolly Kids Meal",
                  "JKM Yum",
                  "JKM Burger"
                ]
              }
            },
            {
              "id": null,
              "name": {
                "value": "Jolly Kids Meal Chicken Joy",
                "synonyms": [
                  "JKM",
                  "Jolly Kiddie Meal",
                  "Jolly Kids Meal",
                  "Chicken",
                  "Chicken Joy",
                  "ChickenJoy",
                  "Kiddie Chicken",
                  "Kiddie Chicken Joy",
                  "Kids Chicken",
                  "Kids Chicken Joy",
                  "Jolly Kids Meal",
                  "JKM Chicken",
                  "JKM Chicken Joy"
                ]
              }
            }
          ]
        }
      ],
      "intents": [
        {
          "name": "AMAZON.CancelIntent",
          "samples": []
        },
        {
          "name": "AMAZON.HelpIntent",
          "samples": []
        },
        {
          "name": "AMAZON.NoIntent",
          "samples": []
        },
        {
          "name": "AMAZON.StopIntent",
          "samples": []
        },
        {
          "name": "AMAZON.YesIntent",
          "samples": []
        },
        {
          "name": "GetFoodSizeIntent",
          "samples": [
            "{foodSize}",
            "{foodSize} {food}"
          ],
          "slots": [
            {
              "name": "foodSize",
              "type": "FOOD_SIZE",
              "samples": [
                "{foodSize}"
              ]
            },
            {
              "name": "food",
              "type": "JOLLI_MENU"
            }
          ]
        },
        {
          "name": "GetTableNumberIntent",
          "samples": [
            "My table number is {tableNumber}",
            "Table number is {tableNumber}",
            "Table number {tableNumber}",
            "number {tableNumber}",
            "table {tableNumber}",
            "{tableNumber}"
          ],
          "slots": [
            {
              "name": "tableNumber",
              "type": "AMAZON.NUMBER",
              "samples": [
                "{tableNumber}",
                "Table number is {tableNumber}",
                "My Table number is {tableNumber}",
                "Table {tableNumber}"
              ]
            }
          ]
        },
        {
          "name": "OrderIntent",
          "samples": [
            "Give me {food}",
            "I am going to order {food}",
            "I am ordering {food}",
            "Please give me {food}",
            "{food}",
            "I need {food}",
            "Please give me {foodCount} {food}",
            "I need {foodCount} {food}",
            "{foodCount} {food}",
            "Give me {foodCount} {food}",
            "{foodCount} {foodSize} {food}",
            "{foodSize} {food}",
            "I need {foodSize} {food}",
            "I need {foodCount} {foodSize} {food}",
            "Give me {foodSize} {food}",
            "Give me {foodCount} {foodSize} {food}"
          ],
          "slots": [
            {
              "name": "foodCount",
              "type": "AMAZON.NUMBER",
              "samples": [
                "{foodCount}",
                "{foodCount} {food}",
                "Give me {foodCount}",
                "Just {foodCount}"
              ]
            },
            {
              "name": "food",
              "type": "JOLLI_MENU",
              "samples": [
                "{food}",
                "I'd like a {food}",
                "{foodCount} {food}"
              ]
            },
            {
              "name": "foodSize",
              "type": "FOOD_SIZE"
            }
          ]
        }
      ],
      "invocationName": "jolly waiter"
    },
    "prompts": [
      {
        "id": "Elicit.Intent-GetFoodSizeIntent.IntentSlot-foodSize",
        "variations": [
          {
            "type": "PlainText",
            "value": "What size do you want?"
          }
        ]
      },
      {
        "id": "Confirm.Intent-GetTableNumberIntent",
        "variations": [
          {
            "type": "PlainText",
            "value": "Your table number is {tableNumber}. Is this correct?"
          }
        ]
      },
      {
        "id": "Elicit.Intent-GetTableNumberIntent.IntentSlot-tableNumber",
        "variations": [
          {
            "type": "PlainText",
            "value": "What is your table number?"
          }
        ]
      },
      {
        "id": "Confirm.Intent-GetTableNumberIntent.IntentSlot-tableNumber",
        "variations": [
          {
            "type": "PlainText",
            "value": "You are in Table {tableNumber}. Is this correct?"
          }
        ]
      },
      {
        "id": "Elicit.Intent-OrderIntent.IntentSlot-foodCount",
        "variations": [
          {
            "type": "PlainText",
            "value": "How many {food}?"
          }
        ]
      },
      {
        "id": "Elicit.Intent-OrderIntent.IntentSlot-food",
        "variations": [
          {
            "type": "PlainText",
            "value": "What would you like?"
          },
          {
            "type": "PlainText",
            "value": "What is your order"
          }
        ]
      }
    ],
    "dialog": {
      "intents": [
        {
          "name": "GetFoodSizeIntent",
          "confirmationRequired": false,
          "prompts": {},
          "slots": [
            {
              "name": "foodSize",
              "type": "FOOD_SIZE",
              "elicitationRequired": true,
              "confirmationRequired": false,
              "prompts": {
                "elicitation": "Elicit.Intent-GetFoodSizeIntent.IntentSlot-foodSize"
              }
            },
            {
              "name": "food",
              "type": "JOLLI_MENU",
              "elicitationRequired": false,
              "confirmationRequired": false,
              "prompts": {}
            }
          ]
        },
        {
          "name": "GetTableNumberIntent",
          "confirmationRequired": true,
          "prompts": {
            "confirmation": "Confirm.Intent-GetTableNumberIntent"
          },
          "slots": [
            {
              "name": "tableNumber",
              "type": "AMAZON.NUMBER",
              "elicitationRequired": true,
              "confirmationRequired": true,
              "prompts": {
                "elicitation": "Elicit.Intent-GetTableNumberIntent.IntentSlot-tableNumber",
                "confirmation": "Confirm.Intent-GetTableNumberIntent.IntentSlot-tableNumber"
              }
            }
          ]
        },
        {
          "name": "OrderIntent",
          "confirmationRequired": false,
          "prompts": {},
          "slots": [
            {
              "name": "foodCount",
              "type": "AMAZON.NUMBER",
              "elicitationRequired": true,
              "confirmationRequired": false,
              "prompts": {
                "elicitation": "Elicit.Intent-OrderIntent.IntentSlot-foodCount"
              }
            },
            {
              "name": "food",
              "type": "JOLLI_MENU",
              "elicitationRequired": true,
              "confirmationRequired": false,
              "prompts": {
                "elicitation": "Elicit.Intent-OrderIntent.IntentSlot-food"
              }
            },
            {
              "name": "foodSize",
              "type": "FOOD_SIZE",
              "elicitationRequired": false,
              "confirmationRequired": false,
              "prompts": {}
            }
          ]
        }
      ]
    }
  }