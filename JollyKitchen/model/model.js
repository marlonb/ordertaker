{
    "languageModel": {
      "intents": [
        {
          "name": "AMAZON.CancelIntent",
          "samples": []
        },
        {
          "name": "AMAZON.HelpIntent",
          "samples": []
        },
        {
          "name": "AMAZON.StopIntent",
          "samples": []
        },
        {
          "name": "GetPendingOrdersIntent",
          "samples": [
            "Give me all pending orders",
            "Give me all the pending orders",
            "Pending orders"
          ],
          "slots": []
        },
        {
          "name": "SetOrdersServedIntent",
          "samples": [
            "Orders completed for table number {tableNumber} order number {orderNumber}",
            "Orders completed for table {tableNumber} order {orderNumber}",
            "Orders completed for table {tableNumber}",
            "Orders completed for order {orderNumber}",
            "Orders completed for table number {tableNumber}",
            "Orders completed for order number {orderNumber}",
            "Order completed for table number {tableNumber} order number {orderNumber}",
            "Order completed for table {tableNumber} order {orderNumber}",
            "Order completed for table {tableNumber}",
            "Order completed for order {orderNumber}",
            "Order completed for table number {tableNumber}",
            "Order completed for order number {orderNumber}",
            "Orders served for table number {tableNumber} order number {orderNumber}",
            "Orders served for table {tableNumber} order {orderNumber}",
            "Orders served for table {tableNumber}",
            "Orders served for order {orderNumber}",
            "Orders served for table number {tableNumber}",
            "Orders served for order number {orderNumber}",
            "Order served for table number {tableNumber} order number {orderNumber}",
            "Order served for table {tableNumber} order {orderNumber}",
            "Order served for table {tableNumber}",
            "Order served for order {orderNumber}",
            "Order served for table number {tableNumber}",
            "Order served for order number {orderNumber}"
          ],
          "slots": [
            {
              "name": "tableNumber",
              "type": "AMAZON.NUMBER",
              "samples": [
                "{tableNumber}",
                "table {tableNumber}"
              ]
            },
            {
              "name": "orderNumber",
              "type": "AMAZON.NUMBER",
              "samples": [
                "{orderNumber}",
                "order {orderNumber}"
              ]
            }
          ]
        }
      ],
      "invocationName": "jolly kitchen"
    },
    "prompts": [
      {
        "id": "Elicit.Intent-SetOrdersServedIntent.IntentSlot-tableNumber",
        "variations": [
          {
            "type": "PlainText",
            "value": "What table did you serve?"
          }
        ]
      },
      {
        "id": "Elicit.Intent-SetOrdersServedIntent.IntentSlot-orderNumber",
        "variations": [
          {
            "type": "PlainText",
            "value": "What order number did you serve?"
          }
        ]
      }
    ],
    "dialog": {
      "intents": [
        {
          "name": "SetOrdersServedIntent",
          "confirmationRequired": false,
          "prompts": {},
          "slots": [
            {
              "name": "tableNumber",
              "type": "AMAZON.NUMBER",
              "elicitationRequired": true,
              "confirmationRequired": false,
              "prompts": {
                "elicitation": "Elicit.Intent-SetOrdersServedIntent.IntentSlot-tableNumber"
              }
            },
            {
              "name": "orderNumber",
              "type": "AMAZON.NUMBER",
              "elicitationRequired": true,
              "confirmationRequired": false,
              "prompts": {
                "elicitation": "Elicit.Intent-SetOrdersServedIntent.IntentSlot-orderNumber"
              }
            }
          ]
        }
      ]
    }
  }