'use strict';

const Alexa = require("alexa-sdk");
const AWS = require("aws-sdk");

const APP_ID = "amzn1.ask.skill.da03aafd-ff81-4fc6-b9b4-69ca73366434";
const AWSregion = 'us-east-1'; // us-east-1
const TABLE_NAME = 'JollibeeAutomatedOrdersDb';
const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });


const returnDefaultEvent = (event) => Object.assign({}, {
        request: {
            locale: 'en-US',
            type: 'LaunchRequest'
        },
        session: {
            application: {
                applicationId: 'applicationId'
            },
            user: {
                userId: 'userId'
            }
        }
    },
    event
);


exports.handler = (event, context) => {
    AWS.config.update({ region: AWSregion });
    const alexa = Alexa.handler(returnDefaultEvent(event), context);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};




const handlers = {
    //By having a single 'Unhandled' handler, we ensure all requests are route to it
    'Unhandled': function() {
        //log the event sent by the Alexa Service in human readable format
        console.log(JSON.stringify(this.event));
        let skillId, requestType, dialogState, intent, intentName, intentConfirmationStatus, slotArray, slots, count;

        try {
            //Parse necessary data from JSON object using dot notation
            //build output strings and check for undefined
            skillId = this.event.session.application.applicationId;
            requestType = "The request type is, " + this.event.request.type + " .";
            dialogState = this.event.request.dialogState;
            intent = this.event.request.intent;
            if (intent != undefined) {
                intentName = " The intent name is, " + this.event.request.intent.name + " .";
                slotArray = this.event.request.intent.slots;
                intentConfirmationStatus = this.event.request.intent.confirmationStatus;

                if (intentConfirmationStatus != "NONE" && intentConfirmationStatus != undefined) {
                    intentConfirmationStatus = " and its confirmation status is " + intentConfirmationStatus + " . ";
                    intentName = intentName + intentConfirmationStatus;
                }
            }
            else {
                intentName = "";
                slotArray = "";
                intentConfirmationStatus = "";
            }

            slots = "";
            count = 0;

            if (slotArray == undefined || slots == undefined) {
                slots = "";
            }

            //Iterating through slot array
            for (let slot in slotArray) {
                count += 1;
                let slotName = slotArray[slot].name;
                let slotValue = slotArray[slot].value;
                let slotConfirmationStatus = slotArray[slot].confirmationStatus;
                slots = slots + "The <say-as interpret-as='ordinal'>" + count + "</say-as> slot is, " + slotName + ", its value is, " + slotValue;

                if (slotConfirmationStatus != undefined && slotConfirmationStatus != "NONE") {
                    slots = slots + " and its confirmation status is " + slotConfirmationStatus + " . ";
                }
                else {
                    slots = slots + " . ";
                }
            }

            //Delegate to Dialog Manager when needed
            //<reference to docs>
            if (dialogState == "STARTED" || dialogState == "IN_PROGRESS") {
                this.emit(":delegate");
            }
        }
        catch (err) {
            console.log("Error: " + err.message);
        }

        let speechOutput = "Your end point received a request, here's a breakdown. " + requestType + " " + intentName + slots;
        let cardTitle = "Skill ID: " + skillId;
        let cardContent = speechOutput;

        this.response.cardRenderer(cardTitle, cardContent);
        this.response.speak(speechOutput);
        this.emit(':responseReady');
    },
    'SetOrdersServedIntent': function() {
        console.log("in SetOrdersServedIntent");

        //delegate to Alexa to collect all the required slot values
        let filledSlots = delegateSlotCollection.call(this);


        let tableNumber = getTableNumberFromSlot.call(this);
        let orderNumber = getOrderNumberFromSlot.call(this);

        if (tableNumber > 0 && orderNumber > 0) {
            setOrdersCompleted.call(this, tableNumber, orderNumber);
        }
        else {
            this.response.speak("Bye");
            this.emit(':responseReady');
        }
    },
    'GetPendingOrdersIntent': function() {
        console.log("in GetPendingOrdersIntent");
        let $this = this;
        getPendingOrders.call(this, function(err, data) {
            let msg = "";
            if (err) {
                msg = "I'm sorry. There was an error encountered in reading orders. Please try again later";
            }
            else {
                if (data) {
                    console.log("ORDERS: " + JSON.stringify(data));
                }
                let orderLength = data.Count;
                if (orderLength && orderLength > 0) {
                    msg = "There are pending orders. ";
                    for (let i = 0; i < orderLength; i++) {
                        let item = data.Items[i];
                        msg += "Order number " + item.OrderNumber.N + ". for table number " + item.TableNumber.N + ". ";
                        let orders = JSON.parse(item.Order.S);
                        for (let j = 0; j < orders.length; j++) {
                            let order = orders[j];
                            msg += order.foodCount + ", ";
                            if (order.foodSize) {
                                msg += order.foodSize + ", ";
                            }
                            msg += order.food.name + ". ";
                        }

                    }
                    msg += " Goodbye!";
                }
                else {
                    msg = "There are no pending orders.";
                }
            }
            $this.response.speak(msg);
            $this.emit(':responseReady');
        });

    },
    'LaunchRequest': function() {
        //setTableNumberToSession.call(this, 0);
        //initFinalOrderListInSession.call(this);
        this.emit('WelcomeKitchen');
    },
    'WelcomeKitchen': function() {
        this.response.speak("Hi. Welcome to Jolly Kitchen. How can I serve you today?").listen();
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function() {
        //orderOutput = "";
        // welcomeReprompt = "";
        //this.response.speak(orderOutput).listen(welcomeReprompt);
        this.response.speak('AMAZON help Intent');
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function() {
        //orderOutput = "";
        this.response.speak('AMAZON cancel Intent');
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function() {
        // orderOutput = "";
        this.response.speak('AMAZON Stop Intent');
        this.emit(':responseReady');
    },
    'SessionEndedRequest': function() {
        var speechOutput = "";
        this.response.speak('Session Ended Request');
        this.emit(':responseReady');
    },
};

function getTableNumberFromSlot() {
    return this.event.request.intent.slots.tableNumber && this.event.request.intent.slots.tableNumber.value ? this.event.request.intent.slots.tableNumber.value : 0;
}

function getOrderNumberFromSlot() {
    return this.event.request.intent.slots.orderNumber && this.event.request.intent.slots.orderNumber.value ? this.event.request.intent.slots.orderNumber.value : 0;
}

function setOrdersCompleted(tableNumber, orderNumber) {
    console.log("this: " + JSON.stringify(this));
    let $this = this;
    dynamodb.query({
        TableName: TABLE_NAME,
        KeyConditionExpression: "ActiveYn = :a and CreatedDateInMilli > :t",
        FilterExpression: "TableNumber = :tn and OrderNumber = :o and OrderStatus = :s",
        ExpressionAttributeValues: {
            ":a": { N: "1"},
            ":s": { S: "ORDERED" },
            ":t": { N: "0" },
            ":tn": { N: tableNumber.toString() },
            ":o": { N: orderNumber.toString() }
        },
        ProjectionExpression: "CreatedDateInMilli"
    }, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        }
        else {
            if (data && data.Count && data.Count === 1) {
                let createdDateInMilli = data.Items[0].CreatedDateInMilli.N;
                
                //update the order status
                dynamodb.updateItem({
                    TableName: TABLE_NAME,
                    Key: {
                        "ActiveYn": { N: "1" },
                        "CreatedDateInMilli": { N: createdDateInMilli.toString() }
                    },
                    UpdateExpression: "set OrderStatus = :newStatus",
                    ExpressionAttributeValues: {
                        ":newStatus": { S: "SERVED"},
                    },
                    ReturnValues: "ALL_NEW"
                }, function(err1, data1) {
                    if (err1) {
                        console.log(err1, err1.stack);
                        $this.response.speak("Error serving order");
                        $this.emit(':responseReady');
                    }
                    else {
                        $this.response.speak("Order Number " + orderNumber + " was SERVED.");
                        $this.emit(':responseReady');
                    }
                });
            }
            else {
                $this.response.speak("There are no orders");
                $this.emit(':responseReady');
            }
        }
    });
}

function getPendingOrders(callback) {

    //let userId = this.event.session.user.userId;
    //console.log("in userId: " + userId);

    dynamodb.query({
        TableName: TABLE_NAME,
        KeyConditionExpression: "ActiveYn = :a and CreatedDateInMilli > :t",
        FilterExpression: "OrderStatus = :ordered",
        ExpressionAttributeValues: {
            ":a": { N: "1"},
            ":t": { N: "0" },
            ":ordered": { S: "ORDERED" }
        }
    }, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        }
        else {

        }
        if (callback) {
            callback(err, data);
        }
    });
}

function delegateSlotCollection() {
    console.log("in delegateSlotCollection");
    console.log("current dialogState: " + this.event.request.dialogState);
    if (this.event.request.dialogState === "STARTED") {
        console.log("in Beginning");
        var updatedIntent = this.event.request.intent;
        console.log("in STARTED: " + JSON.stringify(updatedIntent));
        //optionally pre-fill slots: update the intent object with slot values for which
        //you have defaults, then return Dialog.Delegate with this updated intent
        // in the updatedIntent property
        this.emit(":delegate", updatedIntent);
    }
    else if (this.event.request.dialogState !== "COMPLETED") {
        console.log("in not completed");
        console.log("in !== COMPLETED: " + JSON.stringify(this.event.request.intent));
        // return a Dialog.Delegate directive with no updatedIntent property.
        this.emit(":delegate");
    }
    else {
        console.log("in completed");
        console.log("returning: " + JSON.stringify(this.event.request.intent));
        // Dialog is now complete and all required slots should be filled,
        // so call your normal intent handler.
        return this.event.request.intent;
    }
}